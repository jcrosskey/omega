=================================================================
0. Preprocessing
=================================================================
User should trim, filter, and correct errors of reads 
(sequencing datasets). Please refer the Omega paper to 
preprocess the reads.


=================================================================
1. Installation
=================================================================

Extract the package. 
$ tar xzvf omega-version.tar.gz

Let $OMEGADIR be called as the root directory.
Go into $OMEGADIR/src/debug directory
$ cd omega/src/Debug

Build using makefile. Just type
$ make

Move the "omega" binary file to the root directory
$ mv omega ../../

Set enviroment path of the omega directory. 
E.g., if you use bash shell, add the below line to your ~/.bashrc
export PATH=$OMEGADIR:${PATH}


=================================================================
2. How to run
=================================================================

  Usage:
    omega [OPTION]...<PARAM>...

  <PARAM>
    -pe paired-end file names (comma separated)
    -se single-end file names (comma separated)
    -l  minimum overlap length

  [OPTION]
    -h/--help
    -f  All file name prefix (default: output)
    -s  start from unitig graph


=================================================================
3. Run example
=================================================================

If you have a metagenomic reads set "data1.fastq" and 
"data2.fasq" composed with 100bp length paired-ends reads, run 
as below:

$ omega -pe data1.fastq,data2.fastq -l 60

60bp overlap length might be suitable for 100bp reads.
